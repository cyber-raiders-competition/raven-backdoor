package main

// Majority of this code is from this URL:
// https://coderwall.com/p/wohavg/creating-a-simple-tcp-server-in-go

import (
	"fmt"
	"net"
	"os"
)

const (
	CONN_HOST = "0.0.0.0"
	CONN_PORT = "13370"
	CONN_TYPE = "tcp"
)

func main() {
	// Listen for incoming connections.
	l, err := net.Listen(CONN_TYPE, CONN_HOST+":"+CONN_PORT)

	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	fmt.Println("Listening on " + CONN_HOST + ":" + CONN_PORT)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn)
	}
}

func compareBufs(bufA []byte, bufB []byte, length int) bool {
	for i := 0; i < length; i++ {
		t := bufA[i]

		if t >= 48 && t != bufB[i] {
			return false
		} else if t < 48 {
			return true
		}
	}

	return true
}

// Handles incoming requests.
func handleRequest(conn net.Conn) {
	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)
	// Read the incoming connection into the buffer.

	conn.Write([]byte("Welcome to YOU'RE d00m! Made by 'Totally Not A Backdoor Inc.'.\n\nEnter Backdoor Command: "))
	_, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Error reading:", err.Error())
	}

	// This doesn't work. buf is [116, 102, 345, 0, 0, 0, 0, etc]
	if compareBufs(buf, []byte("doom"), 4) {
		conn.Write([]byte("Doomsday has started!\n"))
	} else if compareBufs(buf, []byte("hero"), 4) {
		conn.Write([]byte("You'll never win.\n"))
	} else if compareBufs(buf, []byte("help"), 4) {
		conn.Write([]byte("Victor von Doom is in need of your services. Today, you work for me.\n"))
	} else {
		conn.Write([]byte("Command Not Valid! No man speaks to Doom this way.\n"))
	}

	// Close the connection when you're done with it.
	conn.Close()
}
